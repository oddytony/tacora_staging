<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="/css/uikit.min.css">
<link rel="stylesheet" type="text/css" href="/css/rtl.css">
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.24/css/uikit.min.css" /> --}}
<!-- jQuery is required -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- UIkit JS -->
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.24/js/uikit.min.js"></script> --}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-beta.24/js/uikit-icons.min.js"></script> --}}
<script type="text/javascript" src="/js/uikit.js"></script>
<script type="text/javascript" src="/js/uikit-icons.js"></script>
<script type="text/javascript" src="/js/uikit.icons.min.js"></script>
<script type="text/javascript" src="/js/uikit.min.js"></script>
<script type="text/javascript" src="/jq/jquery-3.2.1.js"></script>

<!-- <link rel="stylesheet" type="text/css" href="test2css.css"> -->
{{-- <link rel="stylesheet" type="text/css" href="css/custom.css"> --}}
{{-- <link href="https://fonts.googleapis.com/css?family=Bellefair" rel="stylesheet">  --}}
    <title>staging tacora</title>
</head>
<body>

<div class="uk-background-norepeat uk-background-center-center uk-height-medium uk-width-1-1 uk-margin-remove-bottom" style="background-image: url(/img/blacground.jpg); height: 100%; font-family: 'Raleway', sans-serif;">
    
        <div  class="">
        <nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
        <div class="uk-navbar-left">

            <a class="uk-navbar-item uk-logo" href="#"><img style="height:55px ; width: 200px;" src="/img/logo.png"></a>
            <a style="color: white; font-size: 30px  " href=""></a>
        </div>

        </nav>
        </div>
                  






<div class="uk-background-cover " style="background-image: url(/img/shootstar.jpg); width: 100%; object-fit: cover;
   ">
    <div class="uk-padding" uk-grid>
      <div class="uk-width-1-2" style="color:white;">
        <div class="uk-width-1-1 uk-text-cente" > <p style="font-size: 70px">Welcome To  Tacora</p></div>
        <div class="" style="font-size: 22px">The largest database of online courses for professional development in Nigeria, with industry relevant courses developed by Subject matter Experts.</div>
       <div class="uk-margin"> <a class="uk-button uk-button-primary" style="scroll-behavior: smooth;" href="#page2">Take a Free Course</a></div>
        

            


        </div>
        <div class="uk-width-1-2"><img src="/img/laptops2.png"></div>
    </div>
</div>

<div class="uk-margin-large">
<a id="page2" style="scroll-behavior: smooth;"></a>
    <p class="uk-text-large uk-text-center uk-margin-bottom" style="font-size: 70px">Take a free course</p>

    <div class="uk-padding-large uk-padding-remove-vertical " uk-grid>
        <div class="uk-width-1-2"><img src="/img/laptopground.jpg"></div>

        <div>
            <p class="uk-text-large">What do you think you know?</p>
            <p class="uk-text-large">Do you know that you don’t know?</p>
            <p class="uk-text-large">Why don’t you know that you know?</p>
            <p class="uk-text-large">Are you confused yet or you don’t know?</p>
            <p class="uk-text-large">Let Tacora tell you what you need to know; <br></p>
<div class="uk-margin-right">
            <a class="uk-button uk-button-primary uk-button-large uk-button-small uk-margin-large" href="#"  type="button" uk-toggle="target: #modal-example">Start</a></div>
            </div>
{{--             <p class="uk-text-muted">*click on the button below to get started</p>
 --}}            
<!-- This is the modal -->
                <div id="modal-example" uk-modal>
                    <div class="uk-modal-dialog uk-modal-body">
                        <form>
                            <fieldset class="uk-fieldset">                                

                                <div class="uk-margin">
                                    <input class="uk-input" type="text" placeholder="First Name">
                                </div>
                                <div class="uk-margin">
                                    <input class="uk-input" type="text" placeholder="Last Name">
                                </div>
                                <div class="uk-margin">
                                    <input class="uk-input" type="text" placeholder="Enter Email">
                                </div>
                            </fieldset>
                        </form>
                            <button class="uk-button uk-button-primary" type="button">Proceed to free course</button>
                        </p>
                    </div>
                </div>
            </div>

{{-- <div><p class="uk-text-large uk-text-bold">The fountain of youth.</p></div> --}} 
         </div>
    </div>
</div>


</div>

<div class="uk-background-cover " style="background-image: url(/img/bluedot.jpg); width: 100%; object-fit: cover;
   ">

  <div class="uk-margin-large-top uk-padding-large uk-padding-remove-horizontal uk-margin-remove-bottom uk-padding-remove-bottom">
       <div style="color:white;" class="uk-text-center uk-margin"> 
   <h1 style="color:white;" >CONTACT US</h1>
   <p>Let drop a line to us & we will be in touch soon. Here goes some simple dummy text. Lorem Ipsum is simply dummy text</p></div>

   <div class="uk-padding-large" uk-grid>
     
       <div style="color:white;" class="uk-text-large uk-text-bold uk-width-1-2">


          <div uk-grid>
              <div><span style="color:orange;" class="uk-margin-small-right" uk-icon="icon: home; ratio:4;"></span></div>
              <div>No 39A Awudu Ekpekha, Off <br> Admiralty way, Lekki Phase 1 <br> Lagos</div>
          </div>

          <div uk-grid>
             <div><span style="color:orange;"  class="uk-margin-small-right" uk-icon="icon: mail; ratio:4;"></span></div> 
             <div>contact@totoal-ascent.com</div>
          </div>

          <div uk-grid>
             <div><span style="color:orange;" class="uk-margin-small-right" uk-icon="icon: user; ratio:4;"></span></div> 
             <div>Tosin Okojie</div>
          </div>

          <div uk-grid>
             <div><span style="color:orange;" class="uk-margin-small-right" uk-icon="icon: phone; ratio:4;"></span></div> 
             <div>080999999999</div>
          </div>

          


           {{-- <div style="font-size" class="uk-margin" uk-grid> <div><span class="uk-margin-small-right" uk-icon="icon: home; ratio:4;"></span></div> <div>No 39A awudu Ekpekha, Off Admiralt way, Lekki Phase 1 Lagos</div></div>
           <div class="uk-margin">contact@totoal-ascent.com</div>
           <div class="uk-margin">080999999999</div>
           <div class="uk-margin">Osas </div> --}}
       </div>

         <div class="uk-card uk-card-defaul uk-width-1-2 ">
           <form class="uk-padding uk-margin-large uk-card-default uk-container" style="border-radius: 20px">
                <fieldset class="uk-fieldset">


                    <div class="uk-margin">
                    <p>Fullname</p>
                        <input class="uk-input" type="text" placeholder="">
                    </div>

                    <div class="uk-margin">
                    <p>Email Address</p>
                        <input class="uk-input" type="text" placeholder="">
                    </div>        

                {{--     <div class="uk-margin">
                    <p>Gender</p>
                        <select class="uk-select" placeholder="">
                            <option>male</option>
                            <option>female</option>
                        </select>
                    </div>
 --}}
                    <div class="uk-margin">
                    <p>Message</p>
                        <textarea class="uk-textarea" rows="5" placeholder=""></textarea>
                    </div>

                  

                    <a class="uk-button uk-button-primary" href="">Submit</a>

                </fieldset>
            </form>
       </div>
   </div>
  </div>

   </div>

 <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
 <script type="text/javascript">
 
 $('a').click(function(){
     $('html, body').animate({
         scrollTop: $( $(this).attr('href') ).offset().top }, 800);
     return false;
 });
 
 </script>
</body>
</html>

